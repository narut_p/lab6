package lab6;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

/**
 * A main class.
 * 
 * @author Narut Poovorakit
 * @version 24.02.2015
 *
 */
public class WordCount {

	/**
	 * a main method.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		WordCounter word = new WordCounter();
		// Web source.
		String FILE_URL = "https://bitbucket.org/skeoop/oop/raw/master/week6/"
				+ "Alice-in-Wonderland.txt";
		final String DELIMS = "[\\s,.\\?\"():;]+";
		try {
			URL url = new URL(FILE_URL);
			InputStream input = url.openStream();
			Scanner scanner = new Scanner(input);
			scanner.useDelimiter(DELIMS);
			while (scanner.hasNext()) {
				word.addWord(scanner.next());
			}
		} catch (IOException e) {
			// prevent the error and it gone bad.
			e.printStackTrace();
		}
		for (int i = 0; i < 20; i++) {
			System.out.println(word.getSortedWords()[i] + " : "
					+ word.getCount(word.getSortedWords()[i]));
		}

	}
}
