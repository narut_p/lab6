package lab6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Narut Poovorakit
 * @version 24.02.2015 An object that have a map that contain a key(word of
 *          Alice in wonderland) and value of it.
 *
 */
public class WordCounter {

	// initilize set and map.
	private Set<String> set;
	private HashMap<String, Integer> map;

	private static int counter = 0;

	/**
	 * Constructing a constructor.
	 */
	public WordCounter() {
		set = new HashSet<String>();
		map = new HashMap<String, Integer>();
	}

	/**
	 * Adding a word.
	 * 
	 * @param word
	 *            is a word that send from WordCount.
	 */
	public void addWord(String word) {
		if (map.containsKey(word.toLowerCase()))
			map.put(word.toLowerCase(), map.get(word.toLowerCase()) + 1);
		else
			map.put(word.toLowerCase(), 1);

	}

	/**
	 * get a key of a map.
	 * 
	 * @return a set(key of a map).
	 */
	public Set<String> getWords() {
		return map.keySet();
	}

	/**
	 * getting a count of word.
	 * 
	 * @param word
	 *            is a word that been send from WordCount.
	 * @return counting of each word.
	 */
	public int getCount(String word) {
		return map.get(word.toLowerCase());
	}

	/**
	 * Sorting a word and put it in the array.
	 * 
	 * @return An array of sorted words.
	 */
	public String[] getSortedWords() {
		ArrayList<String> arrayword = new ArrayList<String>(getWords());
		Collections.sort(arrayword, String.CASE_INSENSITIVE_ORDER);

		return arrayword.toArray(new String[0]);
	}
}
